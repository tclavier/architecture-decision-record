# Architecture Decision Record

## C'est quoi ? 

Un processus de prise de décisions d'entreprise, d'archivage de ces décisions et de partage de ces décisions.

Il s’agit de capturer les raisons qui ont conduit à cette décision, les alternatives envisagées, et les conséquences possibles de la décision.

cf. Sollicitation d'avis de laloux

## Objectifs

* Archiver toutes les décisions de l'entreprise
* Identifier les dépendances entre décisions
* Identifier les acteurs vers lesquels il faut communiquer le changement de décisions

## Comment ça marche ?

Pour toutes les décision que je ne peux pas prendre seul ou dans mon équipe, les décisions qui impactent d'autres personnes, d'autres équipes, les décisions qui demandent l'intervention d'experts, les décisions qui méritent un débat, etc.

Une décision passe au moins dans 3 états avant d'être terminée : 
* Demande / Proposition de décision
* Débat
* Décision prise

![](./flow.svg)

### Demande / Proposition

Pour sortir de l'état `Demande / Proposition` et entrer dans l'état `Débat` il faut au moins : 
* Un porteur
* Un problème clairement énoncé
* identifier les interlocuteurs clés
* définir le protocole de prise de décision
* Définir le type de débat (Écrit / asynchrone vs Oral / synchrone) et le lieu.

### Débat

* Dérouler les débats, soit en présentiel avec les outils d'intelligence collective, soit à distance avec un outil permettant à tout le monde de s'exprimer.
* Prendre la décision.
* Garder une trace des échanges, des conclusions, des ressources présentées et exploitées.
* Identifier les impacts sur les autres ADR et décrire tous les patchs à faire sur les autres ADR.
* Identifier les impacts sur les équipes, le calendrier, le delivery, etc.

### Décision prise

* Partager la décision et publier l'ADR
* Publier les nouvelles versions d'ADR impactées
* Gérer les autres impacts identifiés

## Besoins en outils

* 1 endroit pour archiver les décisions et leurs discussions dans différentes versions
* avec un bon moteur de recherche,
* et au moins 1 lieu pour discuter.

## Annexes

### Exemple de format d'ADR

* **Numéro unique** : Identifiant unique de l’ADR (par exemple, ADR-1)
* **Version** : Un Numéro de version, par exemple la date.
* **Titre** : Un bref résumé de la décision
* **Contexte** : Description de la situation ou du problème qui a conduit à la prise de décision
* **Décision** : La décision elle-même, clairement énoncée
* **Statut** : Le statut actuel de l’ADR (par exemple : proposé, en cours de débat, validé, etc.) ;
* **Conséquences** : Les conséquences positives et négatives de la décision, les implications pour les projets, les implications sur les autres ADR.
* **Références** : Liens vers des ressources externes, telles que la documentation, les articles de blog ou les discussions pertinentes pour la décision.
* **Archive des débats** : qui a dit quoi et quand, quels étaient les arguments, les risques, quelles étaient les autres options, quel a été le protocole de décision, etc.
